from time import sleep
from stabilock4040 import Stabilock4040
import logging

logging.basicConfig(level=logging.INFO)

with Stabilock4040("GPIB0::22::INSTR") as test_set:
    test_set.beep(3)
    test_set.signal_generator_level = -80
    test_set.frequency = 123
    print(test_set.frequency)
    test_set.one_khz_mod_on()
    test_set.am_mod_depth = 30
    test_set.signal_generator_on()
    print(test_set.signal_generator_level)
    sleep(1)
    print(test_set.am_mod_depth)
    print(test_set.dc_voltage)
    print(test_set.measure_af_level)
    print(test_set.measure_af_distortion)
    sleep(1)
    test_set.one_khz_mod_off()
    test_set.signal_generator_off()
