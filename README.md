# stabilock4040

This is package provides a driver for the Schlumberger/Wavetek Stabilock 4040 Communications Test Set.

It uses PyVISA.

Tested using instrument with firmware version 5.18.

## Install
To install the package, run `pip install stabilock4040`

## Example
See *example.py* for an example of using the package.
